#!/bin/bash

# load global variables

envsubst < /usr/local/tomcat/webapps/pubby/WEB-INF/config.dist.ttl > /usr/local/tomcat/webapps/pubby/WEB-INF/config.ttl

/bin/bash -c "catalina.sh run"
