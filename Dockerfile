FROM tomcat:8.5.23-jre8

MAINTAINER Adam Sanchez "a.sanchez75@gmail.com"

ENV VERSION 0.3.3
ENV SPARQL_ENDPOINT "http://dbpedia.org/sparql"
ENV PROJECT_HOMEPAGE_URL "http://dbpedia.org/"
ENV PROJECT_NAME "DBPEDIA"
ENV DEFAULT_RESOURCE "http://dbpedia.org/resource/Wikipedia"
ENV DEFAULT_NAMED_GRAPH "http://dbpedia.org"
ENV COMMON_URI_PREFIX "http://dbpedia.org/resource/"
ENV WEB_RESOURCE_PREFIX "resource/"
ENV PUBBY_ROOT_URL "http://localhost:8080/pubby/"

RUN apt-get update && apt-get install -y wget ant openjdk-8-jdk openjdk-8-jre gettext-base

RUN cd /tmp && \
    wget https://github.com/cygri/pubby/archive/v${VERSION}.zip && \
    unzip v${VERSION}.zip && \
    rm v${VERSION}.zip

RUN cd /tmp/pubby-${VERSION} && ant

ADD config.dist.ttl /tmp/pubby-${VERSION}/webapp/WEB-INF/

RUN cp -a /tmp/pubby-${VERSION}/webapp /usr/local/tomcat/webapps/pubby

#RUN mkdir -p /data/webapps && cp -Rf /usr/local/tomcat/webapps/* /data/webapps
#RUN mkdir -p /data/conf && cp -Rf /usr/local/tomcat/conf/* /data/conf

COPY init.sh /init.sh

WORKDIR /usr/local/tomcat/webapps/pubby

EXPOSE 8080

CMD ["/bin/bash", "/init.sh"]

